import React from 'react'
import Header from './components/Header'
import MemeGenerator from './components/MemeGenerator'
import './styles/Header.css'
import './styles/MemeGenerator.css'


function App() {
  return (
    <div>
      <Header/>
      <MemeGenerator/>
    </div>
  );
}

export default App;
